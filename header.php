<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" class="no-js">
    <head>
        <title> <?php wp_title(' | ',TRUE,'right'); bloginfo('name'); ?> </title>
        <meta charset="utf-8">
        <meta name="description" content="<?php bloginfo('description', 'display'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <?php wp_head(); ?>

        <!--[if lt IE 9]>
            <script src="<?php bloginfo("template_directory"); ?>/assets/ie/js/html5shiv.js" type="text/javascript"></script>
            <script src="<?php bloginfo("template_directory"); ?>/assets/ie/js/respond.js" type="text/javascript"></script>
            <link rel='stylesheet' href='<?php bloginfo("template_directory"); ?>/assets/ie/css/ie.css' type='text/css' media='all' />
        <![endif]-->

        <?php
        echo get_post_meta( get_the_ID() , 'javascript' , 'true' );
        echo get_post_meta( get_the_ID() , 'css' , 'true' );
        ?>
        <!--//noscript message//--><noscript><!--//noscript message//--><div style="font-size: 12px; background-color: #fef5f1; border: 1px solid #ed541d; padding: 10px;"><samp>This site uses JavaScript to accomplish certain tasks and provide additional functionality in your browser. If you are seeing this message, then something has prevented JavaScript from running and may cause issues when using the site. Please enable JavaScript on your web browser.</samp></div></noscript>
    </head>
    <!--[if lt IE 9]> <body <?php body_class('lt-ie9'); ?>> <![endif]-->
    <!--[if gt IE 8]><!-->
    <body <?php body_class(); ?> >
    <!--<![endif]-->

    <div id="uwsearcharea" aria-hidden="true" class="uw-search-bar-container"></div>

    <a id="main-content" href="#main_content" class="screen-reader-shortcut"><?php _e('Skip to main content', 'uwdgh'); ?></a>

    <div id="uw-container">

    <div id="uw-container-inner">

    <?php if (get_option('uwdgh_theme_show_watermark')) : ?>
    <input type="hidden" name="uwdgh_theme_show_watermark_text" value="<?php echo get_option('uwdgh_theme_show_watermark_text'); ?>">
    <?php endif; ?>

    <?php get_template_part('thinstrip'); ?>

    <?php require( get_template_directory() . '/inc/template-functions.php' );
          uw_dropdowns(); ?>
          
    <?php
    $user = wp_get_current_user();
    if ( get_option('uwdgh_theme_show_php_inspector') && ($user->caps['administrator']) ) : ?>
    <?php get_template_part('uwdgh-php-inspector'); ?>
    <?php endif; ?>
