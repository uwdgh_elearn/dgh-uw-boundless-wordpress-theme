<!--//
PHP INSPECTOR
Note! This is a theme file. A theme update will overwrite any changes made.
//-->
<div id="uwdgh-php-inspector">
  <style>
  .uwdgh-php-inspector summary {
    cursor:pointer;
    background-color:#f5f5f5;
  }
  .uwdgh-php-inspector summary:hover,
  .uwdgh-php-inspector summary:focus {
    background-color:#eee;
  }
  .uwdgh-php-inspector details summary,
  .uwdgh-php-inspector details > div {
    padding: 0 14px;
  }
  .uwdgh-php-inspector details details summary {
    padding: 0 28px;
  }
  .uwdgh-php-inspector details details pre{
    padding: 0 28px;
  }
  </style>
  <details class="uwdgh-php-inspector">
    <summary>PHP INSPECTOR</summary>
    <details>
      <summary>phpinfo()</summary>
      <div class="phpinfo table-responsive"><?php echo phpinfo(); ?></div>
    </details>
    <details>
      <summary>Superglobals</summary>
      <details>
        <summary>$http_response_header</summary>
        <?php if ($_SERVER["SCRIPT_URI"]) { file_get_contents($_SERVER["SCRIPT_URI"]); } ?>
        <pre><?php var_dump($http_response_header); ?></pre>
      </details>
      <details>
        <summary>$_SESSION</summary>
        <pre><?php var_dump($_SESSION); ?></pre>
      </details>
      <details>
        <summary>$_SERVER</summary>
        <pre><?php var_dump($_SERVER); ?></pre>
      </details>
      <details>
        <summary>$_ENV</summary>
        <pre><?php var_dump($_ENV); ?></pre>
      </details>
      <details>
        <summary>$_COOKIE</summary>
        <pre><?php var_dump($_COOKIE); ?></pre>
      </details>
      <details>
        <summary>$_REQUEST</summary>
        <pre><?php var_dump($_REQUEST); ?></pre>
      </details>
      <details>
        <summary>$_GET</summary>
        <pre><?php var_dump($_GET); ?></pre>
      </details>
      <details>
        <summary>$_POST</summary>
        <pre><?php var_dump($_POST); ?></pre>
      </details>
    </details>
    <details>
      <summary>WordPress</summary>
      <details>
        <summary>get_queried_object()</summary>
        <pre><?php if ( function_exists('get_queried_object') ) { var_dump(get_queried_object()); } ?></pre>
      </details>
      <details>
        <summary>wp_get_current_user()</summary>
        <pre><?php if ( function_exists('wp_get_current_user') ) { var_dump(wp_get_current_user()); } ?></pre>
      </details>
      <details>
        <summary>get_plugins()</summary>
        <pre><?php if ( function_exists('get_plugins') ) { var_dump(get_plugins()); } ?></pre>
      </details>
      <details>
        <summary>wp_get_theme()</summary>
        <pre><?php if ( function_exists('wp_get_theme') ) { var_dump(wp_get_theme()); } ?></pre>
      </details>
    </details>
    <details>
      <summary>APPLICATION OUTPUT</summary>
      <pre>
<?php
echo '// Show custom output here.<br>';
echo '// e.g. show the return value for WordPress function is_user_logged_in()<br>';

echo 'is_user_logged_in()=><br>';
var_dump(is_user_logged_in());

// ...
// add custom output here
// please note that a theme update will overwrite any customizations to this file
// ...

?>
      </pre>
    </details>
  </details>
  <script>
  jQuery(document).ready(function( $ ) {
    $('.uwdgh-php-inspector .phpinfo style').remove();
  })
  </script>
</div>
<!--// END PHP INSPECTOR //-->
